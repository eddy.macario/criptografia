﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    //Objeto para almacenar las claves TDES
    public class clavesTDES
    {
        public string tdes1 { get; set; }
        public string tdes2 { get; set; }
        public string tdes3 { get; set; }

        public string vector { get; set; }

        public override string ToString()
        {
            return tdes1 + tdes2 + tdes3;
        }
    }
}
