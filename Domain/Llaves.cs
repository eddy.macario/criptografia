﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain
{
    //Clase para almacenar las diferentes llaves de los algoritmos
    public class Llaves
    {
        //Propiedad que guardara la llave privada del algoritmo RSA
        //Propiedad que guardara la llave del algoritmo TDES
        public string llave1 { get; set; }
        //Propiedad que guardara la llave publica del algoritmo RSA
        //Propiedad que guardara el vactor de inicializacion del algoritmo TDES
        public string llave2 { get; set; }
    }
}
