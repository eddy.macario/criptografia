﻿using System;

using System.Security.Cryptography;
namespace Domain
{
    //Objeto para almacenar las claves del algoritmo de RSA
    public class clavesRsa
    {
        public string publica { get; set; }
        public string privada { get; set; }
    }
}
