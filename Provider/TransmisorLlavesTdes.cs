﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Provider
{
    public class TransmisorLlavesTdes : ITransmisor
    {
        public XmlDocument export(Dictionary<string, string> data)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode docNode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(docNode);
            XmlNode rootNode = xmlDoc.CreateElement("root");
            xmlDoc.AppendChild(rootNode);
            XmlNode tdes1 = xmlDoc.CreateElement("tdes1");
            rootNode.AppendChild(tdes1);
            tdes1.InnerText = data["tdes1"];
            XmlNode tdes2 = xmlDoc.CreateElement("tdes2");
            rootNode.AppendChild(tdes2);
            tdes2.InnerText = data["tdes2"];
            XmlNode tdes3 = xmlDoc.CreateElement("tdes3");
            rootNode.AppendChild(tdes3);
            tdes3.InnerText = data["tdes3"];
            XmlNode vector = xmlDoc.CreateElement("iv");
            rootNode.AppendChild(vector);
            vector.InnerText = data["vector"];
            return xmlDoc;
        }

        public Dictionary<string, string> import(XmlDocument xml)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            XmlNode tdes1 = xml.DocumentElement.SelectSingleNode("/root/tdes1");
            result.Add("tdes1", tdes1.InnerText);
            XmlNode tdes2 = xml.DocumentElement.SelectSingleNode("/root/tdes2");
            result.Add("tdes2", tdes2.InnerText);
            XmlNode tdes3 = xml.DocumentElement.SelectSingleNode("/root/tdes3");
            result.Add("tdes3", tdes3.InnerText);
            XmlNode vector = xml.DocumentElement.SelectSingleNode("/root/iv");
            result.Add("vector", vector.InnerText);
            return result;
        }
    }
}
