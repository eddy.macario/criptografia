﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Provider
{
    public interface ITransmisor
    {
        /*
         * Funcion para importar de un xml datos
         * @param xml XmlDocumento documento xml a importar al documento
         * @return Dictionary<string, string> Diccionario de datos importados
         */
        Dictionary<string, string> import(XmlDocument xml);
        /*
         * Funcion para exportar un Diccionario de datos a Xml
         * @param data Dictionary<string, string>  Diccionario con los datos a exportar
         * @return XmlDocumento Documento xml con el formato deseado para la exportacion
         */
        XmlDocument export(Dictionary<string, string> data);
    }
}
