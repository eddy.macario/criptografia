﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Provider
{
    public abstract class AlgoritmoEncriptacionBase
    {
        //Propiedad que tiene el algoritmo a implementar
        public abstract IAlgoritmoEncriptacion Algoritmo { get; set; }
        //Propiedad que contiene las llaves del algoritmo
        public abstract Llaves Llaves { get; set; }
        //Funcion para la generacion de las claves en el algoritmo
        public abstract void GenerateKeys();
        //Funcion de encriptado del algoritmo
        public abstract string Encriptacion(string desencriptar);
        //Funcion de desencriptacion del algoritmo
        public abstract string Desencriptacion(string encriptado);
    }
}
