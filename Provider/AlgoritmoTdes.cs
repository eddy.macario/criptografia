﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Domain;

namespace Provider
{
    public class AlgoritmoTdes : IAlgoritmoEncriptacion
    {
        public string desEncriptacion(string encriptado, Llaves keys)
        {
            byte[] key = providers.hexToAByte(keys.llave1);
            byte[] IV = providers.hexToAByte(keys.llave2);
            byte[] Data = providers.hexToAByte(encriptado);
            try
            {
                MemoryStream msDecrypt = new MemoryStream(Data);
                CryptoStream csDecrypt = new CryptoStream(msDecrypt,
                    new TripleDESCryptoServiceProvider().CreateDecryptor(key, IV),
                    CryptoStreamMode.Read);
                byte[] fromEncrypt = new byte[Data.Length];
                csDecrypt.Read(fromEncrypt, 0, fromEncrypt.Length);
                return new ASCIIEncoding().GetString(fromEncrypt);
            }
            catch (CryptographicException e)
            {
                MessageBox.Show($"Algo salio mal: {e.ToString()}", "Error", MessageBoxButtons.OK);
                return null;
            }
        }

        public byte[] encriptacion(string mensaje, Llaves keys)
        {
            byte[] key = providers.hexToAByte(keys.llave1);
            byte[] IV = providers.hexToAByte(keys.llave2);
            try
            {
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream,
                    new TripleDESCryptoServiceProvider().CreateEncryptor(key, IV),
                    CryptoStreamMode.Write);
                byte[] toEncrypt = new ASCIIEncoding().GetBytes(mensaje);
                cStream.Write(toEncrypt, 0, toEncrypt.Length);
                cStream.FlushFinalBlock();
                byte[] ret = mStream.ToArray();
                cStream.Close();
                mStream.Close();
                return ret;
            }
            catch (CryptographicException e)
            {
                MessageBox.Show($"Algo salio mal: {e.ToString()}", "Error", MessageBoxButtons.OK);
                return null;
            }
        }

        public Llaves generateKeys()
        {
            Llaves tdes = new Llaves();
            using (TripleDESCryptoServiceProvider tDESalg = new TripleDESCryptoServiceProvider())
            {
                tdes.llave1 = providers.ByteArrayToHex(tDESalg.Key);
                tdes.llave2 = providers.ByteArrayToHex(tDESalg.IV);
            }
            return tdes;
        }
    }
}
