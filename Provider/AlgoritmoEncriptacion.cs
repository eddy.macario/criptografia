﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain;

namespace Provider
{
    public class AlgoritmoEncriptacion : AlgoritmoEncriptacionBase
    {
        public override IAlgoritmoEncriptacion Algoritmo { get; set; }
        public override Llaves Llaves { get; set; }

        public AlgoritmoEncriptacion(IAlgoritmoEncriptacion algoritmo)
        {
            this.Llaves = new Llaves();
            this.Algoritmo = algoritmo;
        }
        public override void GenerateKeys()
        {
            this.Llaves = Algoritmo.generateKeys();
        }
        public override string Desencriptacion(string encriptado)
        {
            return Algoritmo.desEncriptacion(encriptado,this.Llaves);
        }

        public override string Encriptacion(string mensaje)
        {
            byte[] encriptado = Algoritmo.encriptacion(mensaje, this.Llaves);
            return providers.ByteArrayToHex(encriptado);
        }

    }
}
