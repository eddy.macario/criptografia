﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Provider
{
    public class ServicioXML
    {
        /**
         * Funcion para lectura de un archivo xml
         * @param path string path del documento xml
         *  @return doc XmlDocument documento xml
         */
        public static XmlDocument ReadXMLFromFile(string path)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                return doc;
            }catch(Exception e)
            {
                MessageBox.Show($"Algo salio mal: {e.ToString()}", "Error", MessageBoxButtons.OK);
                return null;
            }
        }

        /**
         * Funcion para lectura de un string que contiene xml
         * @param xml string documento xml
         *  @return doc XmlDocument documento xml
         */
        public static XmlDocument ReadXMLFromText(string xml)
        {
            try
            {   
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                return doc;
            }catch(Exception e)
            {
                MessageBox.Show($"Algo salio mal: {e.ToString()}", "Error", MessageBoxButtons.OK);
                return null;
            }
}

        /**
         * Funcion para lectura de un string que contiene xml
         * @param xml string documento xml
         *  @return doc XmlDocument documento xml
         */
        public static void WriteXML(XmlDocument doc, string path)
        {
            try {
            doc.Save(path);
            }catch(Exception e)
            {
                MessageBox.Show($"Algo salio mal: {e.ToString()}", "Error", MessageBoxButtons.OK);
                
            }
}


    }
}
