﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Provider
{
    class TransmisorMensaje : ITransmisor
    {
        public XmlDocument export(Dictionary<string, string> data)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode docNode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(docNode);
            XmlNode rootNode = xmlDoc.CreateElement("root");
            xmlDoc.AppendChild(rootNode);
            XmlNode texto = xmlDoc.CreateElement("textoe");
            texto.InnerText = data["mensaje"];
            rootNode.AppendChild(texto);
            return xmlDoc;
        }

        public Dictionary<string, string> import(XmlDocument xml)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            XmlNode texto = xml.DocumentElement.SelectSingleNode("/root/textoe");
            result.Add("mensaje", texto.InnerText);
            return result;
        }
    }
}
