﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Provider
{
    //Interface que debe cumplir los objetos de los algoritmos de Encriptacion.
    public interface IAlgoritmoEncriptacion
    {
        /*Funcion para encriptado del algoritmo
         * @param mensaje string Texto a encriptar
         * @param keys Llaves claves necesarias para el algoritmo
         * @return byte[] Array de bytes con datos encriptados
         */
        byte[] encriptacion(string mensaje, Llaves keys);
        /*Funcion para desencriptar del algoritmo
         * @param mensaje string Texto a desencriptar
         * @param keys Llaves claves necesarias para el algoritmo
         * @return string Texto con datos desencriptados
         */
        string desEncriptacion(string encriptado, Llaves keys);
        /*Funcion para generar claves del algoritmo
         * @return Llaves Objeto que contiene las claves del algoritmo
         */
        Llaves generateKeys();
    }
}
