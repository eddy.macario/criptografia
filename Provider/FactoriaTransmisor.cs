﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Provider
{
    public class FactoriaTransmisor
    {
        public enum Transmisor { rsa,tdes,mensaje };

        public static ITransmisor Factoria(Transmisor alg)
        {
            switch (alg)
            {
                case Transmisor.rsa:
                    return new TransmisorLlavesRsa();
                case Transmisor.tdes:
                    return new TransmisorLlavesTdes();
                case Transmisor.mensaje:
                    return new TransmisorMensaje();
                default:
                    throw new Exception("Transmisor no encontrado");
            }
        }

    }
}
