﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Xml;

namespace Provider
{
    //Clase contenedora de funciones comunes para todo el sistema
    public class providers
    {
        /*
         *Funcion para transformar un array de bytes a una variable string codificada en hexadecimal 
         *@param ba byte[] array de bytes atransformar
         * @return string bytes codificados en hexadecimal
         */
        public static string ByteArrayToHex(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        /*
         * Funcion para obtener un sub array de un array mas grande, el array  se generara 
         * a partir de las posiciones de inicio y final pasadas
         * @param array byte[] array del que se quiere extraer el sub array
         * @param skip int la posicion a partir de la cual se quiere generar el nuevo array
         * @param get int numemro de elementos a tomar del array base, a partir de la posicion de inicio 
         * @return byte[] sub array generado
         */
        public static byte[] getSubset(byte[] array, int skip, int get)
        {
            int length = get + skip;
            length = length > array.Length ? array.Length : length;
            byte[] response = new byte[get];
            int c = 0;
            for (int i = skip; i < length; i++)
            {
                response[c] = array[i];
                c++;
            }
            return response;
        }

        /*
         * Funcion para pasar de hexadecimal a array de bytes
         * @param hexString string texto con hexadecimal
         * @return byte[]
         */
        public static byte[] hexToAByte(string hexString)
        {
            if (hexString.Length % 2 != 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "The binary key cannot have an odd number of digits: {0}", hexString));
            }

            byte[] data = new byte[hexString.Length / 2];
            for (int index = 0; index < data.Length; index++)
            {
                string byteValue = hexString.Substring(index * 2, 2);
                data[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }

            return data;
        }
    }
}
