﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Provider
{
    public class TransmisorLlavesRsa : ITransmisor
    {
        public XmlDocument export(Dictionary<string, string> data)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode docNode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(docNode);
            XmlNode rootNode = xmlDoc.CreateElement("root");
            xmlDoc.AppendChild(rootNode);
            XmlNode publicKey = xmlDoc.CreateElement("clavepublica");
            rootNode.AppendChild(publicKey);
            publicKey.InnerXml = data["publica"];
            if (publicKey.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
                publicKey.RemoveChild(publicKey.FirstChild);
            var clavePublica = publicKey.InnerXml;
            publicKey.InnerXml = null;
            publicKey.InnerText = clavePublica;
            return xmlDoc;
        }

        public Dictionary<string, string> import(XmlDocument xml)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            XmlNode publicKey = xml.DocumentElement.SelectSingleNode("/root/clavepublica");
            result.Add("publica",publicKey.InnerText);
            return result;
        }
    }
}
