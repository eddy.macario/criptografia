﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Domain;

namespace Provider
{
    public class AlgoritmoRsa : IAlgoritmoEncriptacion
    {
        public string desEncriptacion(string encriptado, Llaves keys)
        {
            byte[] DataToDecrypt = providers.hexToAByte(encriptado);
            try
            {
                byte[] decryptedData;
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(512))
                {
                    RSA.FromXmlString(keys.llave1);
                    decryptedData = RSA.Decrypt(DataToDecrypt, true);
                }
                return providers.ByteArrayToHex(decryptedData);
            }
            catch (CryptographicException e)
            {
                MessageBox.Show($"Algo salio mal: {e.ToString()}", "Error", MessageBoxButtons.OK);
                return null;
            }
        }

        public byte[] encriptacion(string mensaje, Llaves keys)
        {
            byte[] DataToEncrypt = providers.hexToAByte(mensaje);
            try
            {
                byte[] encryptedData;
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(512))
                {
                    RSA.FromXmlString(keys.llave2);
                    encryptedData = RSA.Encrypt(DataToEncrypt, true);
                }
                return encryptedData;
            }
            catch (CryptographicException e)
            {
                MessageBox.Show($"Algo salio mal: {e.ToString()}", "Error", MessageBoxButtons.OK);
                return null;
            }
        }

        public Llaves generateKeys()
        {
            Llaves rsa = new Llaves();
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider(512))
            {
                rsa.llave2 = RSA.ToXmlString(false);
                rsa.llave1 = RSA.ToXmlString(true);
            }
            return rsa;
        }
    }
}
