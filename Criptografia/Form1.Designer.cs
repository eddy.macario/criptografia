﻿namespace Criptografia
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.maestroPanel = new System.Windows.Forms.GroupBox();
            this.lblMensajeMaestroDesencriptado = new System.Windows.Forms.Label();
            this.lblMensajeMaestroEncriptado = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnDesencriptarMensaje = new System.Windows.Forms.Button();
            this.btnImportarMensaje = new System.Windows.Forms.Button();
            this.btnExportarTDES = new System.Windows.Forms.Button();
            this.lblTDESEncriptada = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnEncriptarTDES = new System.Windows.Forms.Button();
            this.lblTDESKey = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnGenerarTDES = new System.Windows.Forms.Button();
            this.lblClavePublicaesclavo = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnImportarRSAEsclavo = new System.Windows.Forms.Button();
            this.lblPadrePublica = new System.Windows.Forms.Label();
            this.lblPadrePrivada = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnRSAMaestro = new System.Windows.Forms.Button();
            this.esclavoPanel = new System.Windows.Forms.GroupBox();
            this.btnExportarMensaje = new System.Windows.Forms.Button();
            this.lblMensajeEncriptado = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnEncriparMensaje = new System.Windows.Forms.Button();
            this.txtMensaje = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTDESDesencriptado = new System.Windows.Forms.Label();
            this.lblTDESEncriptado = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnImportTDES = new System.Windows.Forms.Button();
            this.btnDesencriptarTDES = new System.Windows.Forms.Button();
            this.btnExportarRSAtoXML = new System.Windows.Forms.Button();
            this.lblClavePrivada = new System.Windows.Forms.Label();
            this.lblClavePublica = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGenerateKeyRsa = new System.Windows.Forms.Button();
            this.btnMaestro = new System.Windows.Forms.Button();
            this.btnEsclavo = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.maestroPanel.SuspendLayout();
            this.esclavoPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // maestroPanel
            // 
            this.maestroPanel.BackColor = System.Drawing.Color.SteelBlue;
            this.maestroPanel.Controls.Add(this.lblMensajeMaestroDesencriptado);
            this.maestroPanel.Controls.Add(this.lblMensajeMaestroEncriptado);
            this.maestroPanel.Controls.Add(this.label13);
            this.maestroPanel.Controls.Add(this.label12);
            this.maestroPanel.Controls.Add(this.btnDesencriptarMensaje);
            this.maestroPanel.Controls.Add(this.btnImportarMensaje);
            this.maestroPanel.Controls.Add(this.btnExportarTDES);
            this.maestroPanel.Controls.Add(this.lblTDESEncriptada);
            this.maestroPanel.Controls.Add(this.label11);
            this.maestroPanel.Controls.Add(this.btnEncriptarTDES);
            this.maestroPanel.Controls.Add(this.lblTDESKey);
            this.maestroPanel.Controls.Add(this.label10);
            this.maestroPanel.Controls.Add(this.btnGenerarTDES);
            this.maestroPanel.Controls.Add(this.lblClavePublicaesclavo);
            this.maestroPanel.Controls.Add(this.label9);
            this.maestroPanel.Controls.Add(this.btnImportarRSAEsclavo);
            this.maestroPanel.Controls.Add(this.lblPadrePublica);
            this.maestroPanel.Controls.Add(this.lblPadrePrivada);
            this.maestroPanel.Controls.Add(this.label8);
            this.maestroPanel.Controls.Add(this.label7);
            this.maestroPanel.Controls.Add(this.btnRSAMaestro);
            this.maestroPanel.Location = new System.Drawing.Point(16, 79);
            this.maestroPanel.Margin = new System.Windows.Forms.Padding(4);
            this.maestroPanel.Name = "maestroPanel";
            this.maestroPanel.Padding = new System.Windows.Forms.Padding(4);
            this.maestroPanel.Size = new System.Drawing.Size(1277, 460);
            this.maestroPanel.TabIndex = 0;
            this.maestroPanel.TabStop = false;
            this.maestroPanel.Text = "Maestro";
            this.maestroPanel.Visible = false;
            // 
            // lblMensajeMaestroDesencriptado
            // 
            this.lblMensajeMaestroDesencriptado.AutoSize = true;
            this.lblMensajeMaestroDesencriptado.Location = new System.Drawing.Point(504, 396);
            this.lblMensajeMaestroDesencriptado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMensajeMaestroDesencriptado.Name = "lblMensajeMaestroDesencriptado";
            this.lblMensajeMaestroDesencriptado.Size = new System.Drawing.Size(0, 17);
            this.lblMensajeMaestroDesencriptado.TabIndex = 20;
            // 
            // lblMensajeMaestroEncriptado
            // 
            this.lblMensajeMaestroEncriptado.AutoSize = true;
            this.lblMensajeMaestroEncriptado.Location = new System.Drawing.Point(504, 350);
            this.lblMensajeMaestroEncriptado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMensajeMaestroEncriptado.Name = "lblMensajeMaestroEncriptado";
            this.lblMensajeMaestroEncriptado.Size = new System.Drawing.Size(0, 17);
            this.lblMensajeMaestroEncriptado.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(296, 350);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(151, 17);
            this.label13.TabIndex = 18;
            this.label13.Text = "Mensaje Encriptado";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(296, 396);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(178, 17);
            this.label12.TabIndex = 17;
            this.label12.Text = "Mensaje Desencriptado";
            // 
            // btnDesencriptarMensaje
            // 
            this.btnDesencriptarMensaje.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnDesencriptarMensaje.Location = new System.Drawing.Point(67, 382);
            this.btnDesencriptarMensaje.Margin = new System.Windows.Forms.Padding(4);
            this.btnDesencriptarMensaje.Name = "btnDesencriptarMensaje";
            this.btnDesencriptarMensaje.Size = new System.Drawing.Size(164, 46);
            this.btnDesencriptarMensaje.TabIndex = 16;
            this.btnDesencriptarMensaje.Text = "Desencriptar Mensaje";
            this.btnDesencriptarMensaje.UseVisualStyleBackColor = false;
            this.btnDesencriptarMensaje.Click += new System.EventHandler(this.BtnDesencriptarMensaje_Click);
            // 
            // btnImportarMensaje
            // 
            this.btnImportarMensaje.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnImportarMensaje.Location = new System.Drawing.Point(67, 329);
            this.btnImportarMensaje.Margin = new System.Windows.Forms.Padding(4);
            this.btnImportarMensaje.Name = "btnImportarMensaje";
            this.btnImportarMensaje.Size = new System.Drawing.Size(164, 46);
            this.btnImportarMensaje.TabIndex = 15;
            this.btnImportarMensaje.Text = "Importar Mensaje";
            this.btnImportarMensaje.UseVisualStyleBackColor = false;
            this.btnImportarMensaje.Click += new System.EventHandler(this.BtnImportarMensaje_Click);
            // 
            // btnExportarTDES
            // 
            this.btnExportarTDES.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnExportarTDES.Location = new System.Drawing.Point(300, 282);
            this.btnExportarTDES.Margin = new System.Windows.Forms.Padding(4);
            this.btnExportarTDES.Name = "btnExportarTDES";
            this.btnExportarTDES.Size = new System.Drawing.Size(164, 46);
            this.btnExportarTDES.TabIndex = 14;
            this.btnExportarTDES.Text = "Exportar Clave Encriptada TDES";
            this.btnExportarTDES.UseVisualStyleBackColor = false;
            this.btnExportarTDES.Click += new System.EventHandler(this.BtnExportarTDES_Click);
            // 
            // lblTDESEncriptada
            // 
            this.lblTDESEncriptada.AutoSize = true;
            this.lblTDESEncriptada.Location = new System.Drawing.Point(523, 242);
            this.lblTDESEncriptada.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTDESEncriptada.Name = "lblTDESEncriptada";
            this.lblTDESEncriptada.Size = new System.Drawing.Size(0, 17);
            this.lblTDESEncriptada.TabIndex = 13;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(296, 242);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(177, 17);
            this.label11.TabIndex = 12;
            this.label11.Text = "Clave TDES Encriptada";
            // 
            // btnEncriptarTDES
            // 
            this.btnEncriptarTDES.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnEncriptarTDES.Location = new System.Drawing.Point(67, 228);
            this.btnEncriptarTDES.Margin = new System.Windows.Forms.Padding(4);
            this.btnEncriptarTDES.Name = "btnEncriptarTDES";
            this.btnEncriptarTDES.Size = new System.Drawing.Size(164, 46);
            this.btnEncriptarTDES.TabIndex = 11;
            this.btnEncriptarTDES.Text = "Encriptar Clave TDES";
            this.btnEncriptarTDES.UseVisualStyleBackColor = false;
            this.btnEncriptarTDES.Click += new System.EventHandler(this.BtnEncriptarTDES_Click);
            // 
            // lblTDESKey
            // 
            this.lblTDESKey.AutoSize = true;
            this.lblTDESKey.Location = new System.Drawing.Point(420, 180);
            this.lblTDESKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTDESKey.Name = "lblTDESKey";
            this.lblTDESKey.Size = new System.Drawing.Size(0, 17);
            this.lblTDESKey.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(296, 180);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 17);
            this.label10.TabIndex = 9;
            this.label10.Text = "Clave TDES";
            // 
            // btnGenerarTDES
            // 
            this.btnGenerarTDES.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnGenerarTDES.Location = new System.Drawing.Point(67, 169);
            this.btnGenerarTDES.Margin = new System.Windows.Forms.Padding(4);
            this.btnGenerarTDES.Name = "btnGenerarTDES";
            this.btnGenerarTDES.Size = new System.Drawing.Size(164, 46);
            this.btnGenerarTDES.TabIndex = 8;
            this.btnGenerarTDES.Text = "Generar clave TDES";
            this.btnGenerarTDES.UseVisualStyleBackColor = false;
            this.btnGenerarTDES.Click += new System.EventHandler(this.BtnGenerarTDES_Click);
            // 
            // lblClavePublicaesclavo
            // 
            this.lblClavePublicaesclavo.AutoSize = true;
            this.lblClavePublicaesclavo.Location = new System.Drawing.Point(504, 123);
            this.lblClavePublicaesclavo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblClavePublicaesclavo.Name = "lblClavePublicaesclavo";
            this.lblClavePublicaesclavo.Size = new System.Drawing.Size(0, 17);
            this.lblClavePublicaesclavo.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(296, 123);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(167, 17);
            this.label9.TabIndex = 6;
            this.label9.Text = "Clave Publica Esclavo";
            // 
            // btnImportarRSAEsclavo
            // 
            this.btnImportarRSAEsclavo.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnImportarRSAEsclavo.Location = new System.Drawing.Point(67, 108);
            this.btnImportarRSAEsclavo.Margin = new System.Windows.Forms.Padding(4);
            this.btnImportarRSAEsclavo.Name = "btnImportarRSAEsclavo";
            this.btnImportarRSAEsclavo.Size = new System.Drawing.Size(164, 46);
            this.btnImportarRSAEsclavo.TabIndex = 5;
            this.btnImportarRSAEsclavo.Text = "Importar clave publica RSA esclavo";
            this.btnImportarRSAEsclavo.UseVisualStyleBackColor = false;
            this.btnImportarRSAEsclavo.Click += new System.EventHandler(this.BtnImportarRSAEsclavo_Click);
            // 
            // lblPadrePublica
            // 
            this.lblPadrePublica.AutoSize = true;
            this.lblPadrePublica.Location = new System.Drawing.Point(463, 28);
            this.lblPadrePublica.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPadrePublica.Name = "lblPadrePublica";
            this.lblPadrePublica.Size = new System.Drawing.Size(0, 17);
            this.lblPadrePublica.TabIndex = 4;
            // 
            // lblPadrePrivada
            // 
            this.lblPadrePrivada.AutoSize = true;
            this.lblPadrePrivada.Location = new System.Drawing.Point(463, 78);
            this.lblPadrePrivada.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPadrePrivada.Name = "lblPadrePrivada";
            this.lblPadrePrivada.Size = new System.Drawing.Size(0, 17);
            this.lblPadrePrivada.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(296, 78);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Clave Privada";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(296, 28);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Clave Publica";
            // 
            // btnRSAMaestro
            // 
            this.btnRSAMaestro.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnRSAMaestro.Location = new System.Drawing.Point(67, 28);
            this.btnRSAMaestro.Margin = new System.Windows.Forms.Padding(4);
            this.btnRSAMaestro.Name = "btnRSAMaestro";
            this.btnRSAMaestro.Size = new System.Drawing.Size(164, 46);
            this.btnRSAMaestro.TabIndex = 0;
            this.btnRSAMaestro.Text = "Generar claves RSA";
            this.btnRSAMaestro.UseVisualStyleBackColor = false;
            this.btnRSAMaestro.Click += new System.EventHandler(this.BtnRSAMaestro_Click);
            // 
            // esclavoPanel
            // 
            this.esclavoPanel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.esclavoPanel.Controls.Add(this.btnLimpiar);
            this.esclavoPanel.Controls.Add(this.btnExportarMensaje);
            this.esclavoPanel.Controls.Add(this.lblMensajeEncriptado);
            this.esclavoPanel.Controls.Add(this.label6);
            this.esclavoPanel.Controls.Add(this.btnEncriparMensaje);
            this.esclavoPanel.Controls.Add(this.txtMensaje);
            this.esclavoPanel.Controls.Add(this.label5);
            this.esclavoPanel.Controls.Add(this.lblTDESDesencriptado);
            this.esclavoPanel.Controls.Add(this.lblTDESEncriptado);
            this.esclavoPanel.Controls.Add(this.label4);
            this.esclavoPanel.Controls.Add(this.label3);
            this.esclavoPanel.Controls.Add(this.btnImportTDES);
            this.esclavoPanel.Controls.Add(this.btnDesencriptarTDES);
            this.esclavoPanel.Controls.Add(this.btnExportarRSAtoXML);
            this.esclavoPanel.Controls.Add(this.lblClavePrivada);
            this.esclavoPanel.Controls.Add(this.lblClavePublica);
            this.esclavoPanel.Controls.Add(this.label2);
            this.esclavoPanel.Controls.Add(this.label1);
            this.esclavoPanel.Controls.Add(this.btnGenerateKeyRsa);
            this.esclavoPanel.Location = new System.Drawing.Point(16, 79);
            this.esclavoPanel.Margin = new System.Windows.Forms.Padding(4);
            this.esclavoPanel.Name = "esclavoPanel";
            this.esclavoPanel.Padding = new System.Windows.Forms.Padding(4);
            this.esclavoPanel.Size = new System.Drawing.Size(1277, 460);
            this.esclavoPanel.TabIndex = 1;
            this.esclavoPanel.TabStop = false;
            this.esclavoPanel.Text = "Esclavo";
            // 
            // btnExportarMensaje
            // 
            this.btnExportarMensaje.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnExportarMensaje.Location = new System.Drawing.Point(67, 340);
            this.btnExportarMensaje.Margin = new System.Windows.Forms.Padding(4);
            this.btnExportarMensaje.Name = "btnExportarMensaje";
            this.btnExportarMensaje.Size = new System.Drawing.Size(181, 34);
            this.btnExportarMensaje.TabIndex = 17;
            this.btnExportarMensaje.Text = "Exportar Mensaje a XML";
            this.btnExportarMensaje.UseVisualStyleBackColor = false;
            this.btnExportarMensaje.Click += new System.EventHandler(this.BtnExportarMensaje_Click);
            // 
            // lblMensajeEncriptado
            // 
            this.lblMensajeEncriptado.AutoSize = true;
            this.lblMensajeEncriptado.Location = new System.Drawing.Point(463, 350);
            this.lblMensajeEncriptado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMensajeEncriptado.Name = "lblMensajeEncriptado";
            this.lblMensajeEncriptado.Size = new System.Drawing.Size(0, 17);
            this.lblMensajeEncriptado.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(296, 350);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "Texto Encriptado";
            // 
            // btnEncriparMensaje
            // 
            this.btnEncriparMensaje.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnEncriparMensaje.Location = new System.Drawing.Point(67, 293);
            this.btnEncriparMensaje.Margin = new System.Windows.Forms.Padding(4);
            this.btnEncriparMensaje.Name = "btnEncriparMensaje";
            this.btnEncriparMensaje.Size = new System.Drawing.Size(181, 34);
            this.btnEncriparMensaje.TabIndex = 14;
            this.btnEncriparMensaje.Text = "Encriptar Mensaje";
            this.btnEncriparMensaje.UseVisualStyleBackColor = false;
            this.btnEncriparMensaje.Click += new System.EventHandler(this.BtnEncriparMensaje_Click);
            // 
            // txtMensaje
            // 
            this.txtMensaje.Location = new System.Drawing.Point(447, 299);
            this.txtMensaje.Margin = new System.Windows.Forms.Padding(4);
            this.txtMensaje.Name = "txtMensaje";
            this.txtMensaje.Size = new System.Drawing.Size(821, 22);
            this.txtMensaje.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(296, 303);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Introducir Texto";
            // 
            // lblTDESDesencriptado
            // 
            this.lblTDESDesencriptado.AutoSize = true;
            this.lblTDESDesencriptado.Location = new System.Drawing.Point(475, 240);
            this.lblTDESDesencriptado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTDESDesencriptado.Name = "lblTDESDesencriptado";
            this.lblTDESDesencriptado.Size = new System.Drawing.Size(0, 17);
            this.lblTDESDesencriptado.TabIndex = 11;
            // 
            // lblTDESEncriptado
            // 
            this.lblTDESEncriptado.AutoSize = true;
            this.lblTDESEncriptado.Location = new System.Drawing.Point(475, 180);
            this.lblTDESEncriptado.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTDESEncriptado.Name = "lblTDESEncriptado";
            this.lblTDESEncriptado.Size = new System.Drawing.Size(0, 17);
            this.lblTDESEncriptado.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(296, 180);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "TDES Encriptado";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(296, 240);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "TDES Desencriptado";
            // 
            // btnImportTDES
            // 
            this.btnImportTDES.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnImportTDES.Location = new System.Drawing.Point(67, 161);
            this.btnImportTDES.Margin = new System.Windows.Forms.Padding(4);
            this.btnImportTDES.Name = "btnImportTDES";
            this.btnImportTDES.Size = new System.Drawing.Size(181, 53);
            this.btnImportTDES.TabIndex = 7;
            this.btnImportTDES.Text = "Importar clave TDES de fichero";
            this.btnImportTDES.UseVisualStyleBackColor = false;
            this.btnImportTDES.Click += new System.EventHandler(this.BtnImportTDES_Click);
            // 
            // btnDesencriptarTDES
            // 
            this.btnDesencriptarTDES.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnDesencriptarTDES.Location = new System.Drawing.Point(67, 222);
            this.btnDesencriptarTDES.Margin = new System.Windows.Forms.Padding(4);
            this.btnDesencriptarTDES.Name = "btnDesencriptarTDES";
            this.btnDesencriptarTDES.Size = new System.Drawing.Size(181, 52);
            this.btnDesencriptarTDES.TabIndex = 6;
            this.btnDesencriptarTDES.Text = "Desencriptar TDES de fichero";
            this.btnDesencriptarTDES.UseVisualStyleBackColor = false;
            this.btnDesencriptarTDES.Click += new System.EventHandler(this.BtnDesencriptarTDES_Click);
            // 
            // btnExportarRSAtoXML
            // 
            this.btnExportarRSAtoXML.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnExportarRSAtoXML.Location = new System.Drawing.Point(67, 81);
            this.btnExportarRSAtoXML.Margin = new System.Windows.Forms.Padding(4);
            this.btnExportarRSAtoXML.Name = "btnExportarRSAtoXML";
            this.btnExportarRSAtoXML.Size = new System.Drawing.Size(181, 41);
            this.btnExportarRSAtoXML.TabIndex = 5;
            this.btnExportarRSAtoXML.Text = " Exportar xml con clave publica";
            this.btnExportarRSAtoXML.UseVisualStyleBackColor = false;
            this.btnExportarRSAtoXML.Click += new System.EventHandler(this.BtnExportarRSAtoXML_Click);
            // 
            // lblClavePrivada
            // 
            this.lblClavePrivada.AutoSize = true;
            this.lblClavePrivada.Location = new System.Drawing.Point(451, 106);
            this.lblClavePrivada.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblClavePrivada.Name = "lblClavePrivada";
            this.lblClavePrivada.Size = new System.Drawing.Size(0, 17);
            this.lblClavePrivada.TabIndex = 4;
            this.lblClavePrivada.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblClavePublica
            // 
            this.lblClavePublica.AutoSize = true;
            this.lblClavePublica.Location = new System.Drawing.Point(451, 41);
            this.lblClavePublica.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblClavePublica.Name = "lblClavePublica";
            this.lblClavePublica.Size = new System.Drawing.Size(0, 17);
            this.lblClavePublica.TabIndex = 3;
            this.lblClavePublica.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(296, 94);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Clave Privada";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(296, 41);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Clave Publica";
            // 
            // btnGenerateKeyRsa
            // 
            this.btnGenerateKeyRsa.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnGenerateKeyRsa.Location = new System.Drawing.Point(67, 28);
            this.btnGenerateKeyRsa.Margin = new System.Windows.Forms.Padding(4);
            this.btnGenerateKeyRsa.Name = "btnGenerateKeyRsa";
            this.btnGenerateKeyRsa.Size = new System.Drawing.Size(181, 41);
            this.btnGenerateKeyRsa.TabIndex = 0;
            this.btnGenerateKeyRsa.Text = "Generar claves RSA";
            this.btnGenerateKeyRsa.UseVisualStyleBackColor = false;
            this.btnGenerateKeyRsa.Click += new System.EventHandler(this.BtnGenerateKeyRsa_Click);
            // 
            // btnMaestro
            // 
            this.btnMaestro.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnMaestro.Location = new System.Drawing.Point(419, 9);
            this.btnMaestro.Margin = new System.Windows.Forms.Padding(4);
            this.btnMaestro.Name = "btnMaestro";
            this.btnMaestro.Size = new System.Drawing.Size(175, 63);
            this.btnMaestro.TabIndex = 2;
            this.btnMaestro.Text = "Maestro";
            this.btnMaestro.UseVisualStyleBackColor = false;
            this.btnMaestro.Click += new System.EventHandler(this.BtnMaestro_Click);
            // 
            // btnEsclavo
            // 
            this.btnEsclavo.BackColor = System.Drawing.Color.AliceBlue;
            this.btnEsclavo.Location = new System.Drawing.Point(812, 9);
            this.btnEsclavo.Margin = new System.Windows.Forms.Padding(4);
            this.btnEsclavo.Name = "btnEsclavo";
            this.btnEsclavo.Size = new System.Drawing.Size(175, 63);
            this.btnEsclavo.TabIndex = 3;
            this.btnEsclavo.Text = "Esclavo";
            this.btnEsclavo.UseVisualStyleBackColor = false;
            this.btnEsclavo.Click += new System.EventHandler(this.BtnEsclavo_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnLimpiar.Location = new System.Drawing.Point(1087, 257);
            this.btnLimpiar.Margin = new System.Windows.Forms.Padding(4);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(181, 34);
            this.btnLimpiar.TabIndex = 18;
            this.btnLimpiar.Text = "Limpiar Campos";
            this.btnLimpiar.UseVisualStyleBackColor = false;
            this.btnLimpiar.Click += new System.EventHandler(this.BtnLimpiar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1309, 554);
            this.Controls.Add(this.btnEsclavo);
            this.Controls.Add(this.btnMaestro);
            this.Controls.Add(this.esclavoPanel);
            this.Controls.Add(this.maestroPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Crypto Message";
            this.maestroPanel.ResumeLayout(false);
            this.maestroPanel.PerformLayout();
            this.esclavoPanel.ResumeLayout(false);
            this.esclavoPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox maestroPanel;
        private System.Windows.Forms.GroupBox esclavoPanel;
        private System.Windows.Forms.Button btnMaestro;
        private System.Windows.Forms.Button btnEsclavo;
        private System.Windows.Forms.Label lblClavePrivada;
        private System.Windows.Forms.Label lblClavePublica;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGenerateKeyRsa;
        private System.Windows.Forms.Button btnExportarRSAtoXML;
        private System.Windows.Forms.Label lblTDESDesencriptado;
        private System.Windows.Forms.Label lblTDESEncriptado;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnImportTDES;
        private System.Windows.Forms.Button btnDesencriptarTDES;
        private System.Windows.Forms.Button btnEncriparMensaje;
        private System.Windows.Forms.TextBox txtMensaje;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnExportarMensaje;
        private System.Windows.Forms.Label lblMensajeEncriptado;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnRSAMaestro;
        private System.Windows.Forms.Label lblPadrePublica;
        private System.Windows.Forms.Label lblPadrePrivada;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblClavePublicaesclavo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnImportarRSAEsclavo;
        private System.Windows.Forms.Button btnGenerarTDES;
        private System.Windows.Forms.Label lblTDESKey;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblTDESEncriptada;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnEncriptarTDES;
        private System.Windows.Forms.Button btnExportarTDES;
        private System.Windows.Forms.Label lblMensajeMaestroDesencriptado;
        private System.Windows.Forms.Label lblMensajeMaestroEncriptado;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnDesencriptarMensaje;
        private System.Windows.Forms.Button btnImportarMensaje;
        private System.Windows.Forms.Button btnLimpiar;
    }
}

