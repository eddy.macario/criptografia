﻿using Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Criptografia
{
    public partial class Form1 : Form
    {
        protected EsclavoService esclavo;
        protected MaestroService maestro;
        protected OpenFileDialog ofd;
        protected FolderBrowserDialog fbd;
        public Form1()
        {
            esclavo = new EsclavoService();
            maestro = new MaestroService();
            ofd = new OpenFileDialog();
            fbd = new FolderBrowserDialog();
            InitializeComponent();
        }

        private void BtnMaestro_Click(object sender, EventArgs e)
        {
            esclavoPanel.Visible = false;
            btnEsclavo.BackColor = Color.DodgerBlue;
            btnMaestro.BackColor = Color.AliceBlue;
            maestroPanel.Visible = true;
        }

        private void BtnEsclavo_Click(object sender, EventArgs e)
        {
            esclavoPanel.Visible = true;
            btnMaestro.BackColor = Color.DodgerBlue;
            btnEsclavo.BackColor = Color.AliceBlue;
            maestroPanel.Visible = false;
        }

        private void BtnGenerateKeyRsa_Click(object sender, EventArgs e)
        {
            generateKeys();
        }

        private void BtnExportarRSAtoXML_Click(object sender, EventArgs e)
        {
            if(fbd.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(lblClavePublica.Text))
            {
                MessageBox.Show("Tu archivo se guardo como cp_esclavo.xml en la ubicacion deseada", "Informacion importante", MessageBoxButtons.OK);
                esclavo.writePublicKeyToXML(fbd.SelectedPath + "/cp_esclavo.xml");
            }
        }

        private void BtnImportTDES_Click(object sender, EventArgs e)
        {
            ofd.Filter = "XML Files (*.xml)|*.xml";
            ofd.FilterIndex = 0;
            ofd.DefaultExt = "xml";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try {
                    lblTDESEncriptado.BackColor = Color.White;
                    lblTDESEncriptado.Text = esclavo.importarTdesKeys(ofd.FileName);
                }
                catch (Exception er)
                {
                    MessageBox.Show($"Algo salio mal: {er}", "Error", MessageBoxButtons.OK);
                }
            }
        }

        private void BtnDesencriptarTDES_Click(object sender, EventArgs e)
        {
            try {
                lblTDESDesencriptado.BackColor = Color.White;
                lblTDESDesencriptado.Text = esclavo.desencriptarTdesKeys();
            }
            catch (Exception er)
            {
                MessageBox.Show($"Algo salio mal: {er}", "Error", MessageBoxButtons.OK);
            }
        }

        private void BtnEncriparMensaje_Click(object sender, EventArgs e)
        {
            string mensaje = txtMensaje.Text;
            if (!string.IsNullOrEmpty(mensaje))
            {
                try
                {
                    txtMensaje.Enabled = false;
                    lblMensajeEncriptado.BackColor = Color.White;
                    lblMensajeEncriptado.Text = esclavo.encriptarMensajeTdes(mensaje);
                }
                catch(Exception er)
                {
                    MessageBox.Show($"Algo salio mal: {er}", "Error", MessageBoxButtons.OK);
                }                
            }
            else
            {
                MessageBox.Show("Porfavor ingrese un mensaje para encriptarlo","Error",MessageBoxButtons.OK);
            }
        }

        private void BtnExportarMensaje_Click(object sender, EventArgs e)
        {
            if (fbd.ShowDialog() == DialogResult.OK && !string.IsNullOrEmpty(lblClavePublica.Text))
            {
                esclavo.exportMensaje(fbd.SelectedPath + "/textoencriptado.xml");
                MessageBox.Show("Tu archivo se guardo como textoencriptado.xml en la ubicacion deseada", "Informacion importante", MessageBoxButtons.OK);
            }
        }

        private void BtnRSAMaestro_Click(object sender, EventArgs e)
        {
            generateKeys();
        }

        private void generateKeys()
        {
            esclavo.generarClavesRSA();
            lblClavePrivada.BackColor = Color.White;
            lblClavePublica.BackColor = Color.White;
            lblPadrePublica.BackColor = Color.White;
            lblPadrePrivada.BackColor = Color.White;
            lblClavePrivada.Text = esclavo.ClavePrivada;
            lblClavePublica.Text = esclavo.ClavePublica;
            lblPadrePublica.Text = RandomString(64,false);
            lblPadrePrivada.Text = RandomString(64, false);
        }

        private string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 1; i < size + 1; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            else
                return builder.ToString();
        }

        private void BtnImportarRSAEsclavo_Click(object sender, EventArgs e)
        {
            ofd.Filter = "XML Files (*.xml)|*.xml";
            ofd.FilterIndex = 0;
            ofd.DefaultExt = "xml";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    lblClavePublicaesclavo.BackColor = Color.White;
                    lblClavePublicaesclavo.Text = maestro.importPublicKey(ofd.FileName);
                }
                catch (Exception er)
                {
                    MessageBox.Show($"Algo salio mal: {er}", "Error", MessageBoxButtons.OK);
                }
            }
        }

        private void BtnGenerarTDES_Click(object sender, EventArgs e)
        {
            lblTDESKey.BackColor = Color.White;
           lblTDESKey.Text = maestro.generateTdesKeys();
        }

        private void BtnEncriptarTDES_Click(object sender, EventArgs e)
        {
            try
            {
                lblTDESEncriptada.BackColor = Color.White;
                lblTDESEncriptada.Text = maestro.encriptarTdesKeys();
            }
            catch (Exception er)
            {
                MessageBox.Show($"Algo salio mal: {er}", "Error", MessageBoxButtons.OK);
            }
        }

        private void BtnExportarTDES_Click(object sender, EventArgs e)
        {
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                MessageBox.Show("Tu archivo se guardo como tdesencriptado.xml en la ubicacion deseada", "Informacion importante", MessageBoxButtons.OK);
                maestro.exportTdesKeys(fbd.SelectedPath + "/tdesencriptado.xml");
            }
        }

        private void BtnImportarMensaje_Click(object sender, EventArgs e)
        {
            ofd.Filter = "XML Files (*.xml)|*.xml";
            ofd.FilterIndex = 0;
            ofd.DefaultExt = "xml";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    lblMensajeMaestroEncriptado.BackColor = Color.White;
                    lblMensajeMaestroEncriptado.Text = maestro.importarMensaje(ofd.FileName);
                }
                catch (Exception er)
                {
                    MessageBox.Show($"Algo salio mal: {er}", "Error", MessageBoxButtons.OK);
                }
            }
        }

        private void BtnDesencriptarMensaje_Click(object sender, EventArgs e)
        {
            try
            {
                lblMensajeMaestroDesencriptado.BackColor = Color.White;
                lblMensajeMaestroDesencriptado.Text = maestro.desencriptarMensaje();
            }
            catch (Exception er)
            {
                MessageBox.Show($"Algo salio mal: {er}", "Error", MessageBoxButtons.OK);
            }
        }

        private void BtnLimpiar_Click(object sender, EventArgs e)
        {
            txtMensaje.Enabled = true;
            txtMensaje.Text = null;
            lblMensajeEncriptado.Text = null;
        }
    }
}
