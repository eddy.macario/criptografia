﻿using Domain;
using Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Service
{
    public class EsclavoService
    {
        private AlgoritmoEncriptacionBase algoritmoRsa, algoritmoTdes;
        private ITransmisor transmisorTdes, transmisorRsa, transmisorMensaje;
        public string ClavePublica { get; set; }
        public string ClavePrivada { get; set; }
        protected clavesRsa RsaKeys { get; set; }
        protected clavesTDES tdesKeys { get; set; }
        protected clavesTDES tdesKeysEncrypt { get; set; }
        protected string mensajeEncrypt { get; set; }
        //Clase que contiene las operaciones que desarrollara el servicio del esclavvo
        public EsclavoService()
        {
            algoritmoRsa = new AlgoritmoEncriptacion(new AlgoritmoRsa());
            algoritmoTdes = new AlgoritmoEncriptacion(new AlgoritmoTdes());
            transmisorTdes = FactoriaTransmisor.Factoria(FactoriaTransmisor.Transmisor.tdes);
            transmisorRsa = FactoriaTransmisor.Factoria(FactoriaTransmisor.Transmisor.rsa);
            transmisorMensaje = FactoriaTransmisor.Factoria(FactoriaTransmisor.Transmisor.mensaje);
            RsaKeys = new clavesRsa();
            tdesKeys = new clavesTDES();
            tdesKeysEncrypt = new clavesTDES();
        }
        /*
         * Funcion que genera las claves del algoritmo RSA
         */
        public void generarClavesRSA()
        {
            algoritmoRsa.GenerateKeys();
            RsaKeys.privada = algoritmoRsa.Llaves.llave1;
            RsaKeys.publica = algoritmoRsa.Llaves.llave2;
            XmlNode publica = ServicioXML.ReadXMLFromText(RsaKeys.publica).DocumentElement.SelectSingleNode("/RSAKeyValue/Modulus");
            XmlNode privada = ServicioXML.ReadXMLFromText(RsaKeys.publica).DocumentElement.SelectSingleNode("/RSAKeyValue/Modulus");
            ClavePublica = publica.InnerText;
            ClavePrivada = privada.InnerText;
        }
        /*
         * Funcion que exportara a xml la clave publica del algoritmo RSA 
         * @param path string Ruta donde se almacenara el xml
         */
        public void writePublicKeyToXML(string path)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("publica", RsaKeys.publica);
            XmlDocument xml = transmisorRsa.export(data);
            ServicioXML.WriteXML(xml, path);
        }
        /*
         * Funcion para importar archivo xml que contiene clave y vector inicial del algoritmo TDES
         * @param path string Ruta donde se lozaliza el xml que contiene las claves y vector de inicializacion del algoritmo TDES
         * @return string clave TDES encriptada
         */
        public string importarTdesKeys(string path)
        {
            XmlDocument xml = ServicioXML.ReadXMLFromFile(path);
            Dictionary<string,string> data = transmisorTdes.import(xml);
            tdesKeysEncrypt.tdes1 = data["tdes1"];
            tdesKeysEncrypt.tdes2 = data["tdes2"];
            tdesKeysEncrypt.tdes3 = data["tdes3"];
            tdesKeysEncrypt.vector = data["vector"];
            return tdesKeysEncrypt.ToString();
        }
        /*
         * Funcion que desencriptar la clave y y vector inicial del algoritmo TDES
         * @return string clave desencriptada para el algoritmo TDES
         */
        public string desencriptarTdesKeys()
        {
            tdesKeys.tdes1 = algoritmoRsa.Desencriptacion(tdesKeysEncrypt.tdes1);
            tdesKeys.tdes2 = algoritmoRsa.Desencriptacion(tdesKeysEncrypt.tdes2);
            tdesKeys.tdes3 = algoritmoRsa.Desencriptacion(tdesKeysEncrypt.tdes3);
            tdesKeys.vector = algoritmoRsa.Desencriptacion(tdesKeysEncrypt.vector);
            algoritmoTdes.Llaves.llave1 = tdesKeys.ToString();
            algoritmoTdes.Llaves.llave2 = tdesKeys.vector;
            return tdesKeys.ToString();
        }
        /*
         * Funcion par encriptar mensaje mediante el algoritmo TDES
         * @return string mensaje encriptado
         */
        public string encriptarMensajeTdes(string mensaje)
        {
            mensajeEncrypt = algoritmoTdes.Encriptacion(mensaje);
            return mensajeEncrypt;
        }
        /*
         * Funcion que exporta el mensaje encriptado a archivo xml
         * @param path string Ruta donde se almacenara el xml 
         */
        public void exportMensaje(string path)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("mensaje", mensajeEncrypt);
            XmlDocument xml = transmisorMensaje.export(data);
            ServicioXML.WriteXML(xml,path);
        }
    }
}
