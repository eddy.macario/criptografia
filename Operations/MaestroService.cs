﻿using Domain;
using Provider;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Service
{
    public class MaestroService
    {
        protected string rsaPublicKey { get; set; }
        protected AlgoritmoEncriptacionBase algoritmoRsa,algoritmoTdes;
        protected ITransmisor transmisorTdes, transmisorRsa, transmisorMensaje;
        protected string mensajeEncryptado { get; set; }
        protected clavesTDES tdesKeys { get; set; }
        protected clavesTDES tdesKeysEncrypt { get; set; }
        //Constructor de la clase con el servicio y operaciones que tendra el maestro
        public MaestroService()
        {
            algoritmoRsa = new AlgoritmoEncriptacion(new AlgoritmoRsa());
            algoritmoTdes = new AlgoritmoEncriptacion(new AlgoritmoTdes());
            transmisorTdes = FactoriaTransmisor.Factoria(FactoriaTransmisor.Transmisor.tdes);
            transmisorRsa = FactoriaTransmisor.Factoria(FactoriaTransmisor.Transmisor.rsa);
            transmisorMensaje = FactoriaTransmisor.Factoria(FactoriaTransmisor.Transmisor.mensaje);
            tdesKeys = new clavesTDES();
            tdesKeysEncrypt = new clavesTDES();
        }
        /*
         * Funcion para importar la clave publica del esclavo al maestro
         * @param path string ruta donde esta localizado el xml con la clave publica
         * @return string clave publica importada
         */
        public string importPublicKey(string path)
        {
            XmlDocument xml =  ServicioXML.ReadXMLFromFile(path);
            Dictionary<string,string> result = transmisorRsa.import(xml);
            rsaPublicKey = transmisorRsa.import(xml)["publica"];
            algoritmoRsa.Llaves.llave2 = rsaPublicKey;
            XmlNode publica = ServicioXML.ReadXMLFromText(rsaPublicKey).DocumentElement.SelectSingleNode("/RSAKeyValue/Modulus");
            return publica.InnerText;            
        }
        /*
         * Funcion para generacion de la clave y vector de inicializacion del algoritmo TDES
         * @return string clave tdes generada
         */
        public string generateTdesKeys()
        {
            algoritmoTdes.GenerateKeys();
            string llaveTdes = algoritmoTdes.Llaves.llave1;
            byte[] ALlaveTdes = providers.hexToAByte(llaveTdes);
            this.tdesKeys.tdes1 = providers.ByteArrayToHex(providers.getSubset(ALlaveTdes,0,8));
            this.tdesKeys.tdes2 = providers.ByteArrayToHex(providers.getSubset(ALlaveTdes, 8, 8));
            this.tdesKeys.tdes3 = providers.ByteArrayToHex(providers.getSubset(ALlaveTdes, 16, 8));
            this.tdesKeys.vector = algoritmoTdes.Llaves.llave2;
            return llaveTdes;
        }
        /*
         * Funcion para encriptar la clave y vector de inicializacion del algoritmo TDES
         * @return string clave tdes encriptada
         */

        public string encriptarTdesKeys()
        {
            tdesKeysEncrypt.tdes1 = algoritmoRsa.Encriptacion(tdesKeys.tdes1);
            tdesKeysEncrypt.tdes2 = algoritmoRsa.Encriptacion(tdesKeys.tdes2);
            tdesKeysEncrypt.tdes3 = algoritmoRsa.Encriptacion(tdesKeys.tdes3);
            tdesKeysEncrypt.vector = algoritmoRsa.Encriptacion(tdesKeys.vector); 
            return tdesKeysEncrypt.ToString();
        }
        /*
         * Funcion para exportar las claves TDES a archivo xml
         * @param path string Ruta donde se guardara el archivo xml con las claves TDES con el formato
         */
        public void exportTdesKeys(string path)
        {
            Dictionary<string, string> tdesLlaves = new Dictionary<string, string>();
            tdesLlaves.Add("tdes1",tdesKeysEncrypt.tdes1);
            tdesLlaves.Add("tdes2", tdesKeysEncrypt.tdes2);
            tdesLlaves.Add("tdes3", tdesKeysEncrypt.tdes3);
            tdesLlaves.Add("vector", tdesKeysEncrypt.vector);
            XmlDocument xml = transmisorTdes.export(tdesLlaves);
            ServicioXML.WriteXML(xml,path);
        }
        /*
         * Funcion para importar archivo xml que contiene mensaje encriptado
         * @param path string Ruta donde se localiza el xml que contiene el mensaje encriptado
         */
        public string importarMensaje(string path)
        {
            XmlDocument xml = ServicioXML.ReadXMLFromFile(path);
            Dictionary<string,string> data = transmisorMensaje.import(xml);
            mensajeEncryptado = data["mensaje"];
            return mensajeEncryptado;
        }
        /*
         * Funcion para desencriptar mensaje encriptado
         * @return string mensaje desencriptado
         */
        public string desencriptarMensaje()
        {
            return algoritmoTdes.Desencriptacion(mensajeEncryptado);
        }
    }
}
